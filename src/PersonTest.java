
import java.util.Scanner;

// include any necessary import statements

/**
 * Application (driver) class used to test the capabilities of class Person
 *
 * @author
 * @version
 */
public class PersonTest {

    public static void main(String[] args) {
        // instantiate Scanner object for reading in keyboard input
        Scanner input = new Scanner(System.in );

        // prompt user to input name
        System.out.print( "Enter the first person's name: ");
        
        // read in the name 
        String name1; //local refernce type variables are null by default
        do {
            if (input.hasNextLine() ){
                
                name1 = input.nextLine();
                break;
            
            } else { 
                System.out.println("Error! Invalid name. Try again");
                input.nextLine(); //discards the entiree line (dont assign it to anything)
                continue; //unnecessary 
            } //end if/else
            
            //there are no statements to skip...so...
            
        } while (true);
        
        // prompt user to input SSN
        System.out.print("Enter the first person's nine-digit SSN (no spaces or dashes): ");
        
        // read in the SSN
        int ssn1=0;//initialize cuz locla primitive type variables have no default value 
        boolean isValid = false; //more elegant than using break
        while ( !isValid ) { //"while isvalid in not true..."
            if (input.hasNextInt() ) //did the user type in an int
                ssn1=input.nextInt(); 
                input.nextLine(); //discard other entered data, if any 
                isValid = true; //this allows us to exit the loop gracefully
                
        }
        {
        else {
                System.out.println("error! invalid ssn. try again: ");
                input.nextLine();//discard the entire line(assuming the user didnt type in an int)
            }
        }

        
        // prompt user to input voter registration status as true or false
        System.out.print( "Enter the first person's registration status (true or false):  ");
        
        // read in the voter status 
                do {
            if (input.hasNextBoolean() ){
                
                voterStatus1 = input.nextBoolean();
                input.nextLine();
                break;
            
            } else { 
                System.out.println("Error! Invalid registration status. Try again");
                input.nextLine(); //discards the entiree line (dont assign it to anything)
               // continue; //unnecessary 
            }} //end if/else
                
        
        // prompt user to input annual salary
        System.out.print( "Enter the first person's annual salary as a decimal value: ");
        
        // read in the salary amount
        double salary1 = 0.0;
        
        //use exception handling to read in valid annual salary 
        boolean isValidSalary = false; //will update to true when valid salary has been input 
        
        while (!isValidSalary) { //loop wll continue to repeat until isValidSalary == true
            //prompt user to input salary
            System.out.print("Enter the first person's annual salary as a decimal value: ");
        try {
            //inside the try block, youll code the statements 
            //that could potentially throw an exception
            salary1=input.nextDouble();
            isValidSalary = true; 
        } catch (InputMismatchException e ){
            System.out.println("error! please try again. ");
        }//end try statemwnt
        
        input.nextLine(); //discard rest of line, regardless of whether an exception was thrown
        
        }
            
                
        
        // Create new Person object using the parameters above, and assign 
        // object reference to newly declared person1 object variable
        Person person1 = new Person( name1, ssn1, voterStatus1, salary1 );
        
        // Create new Person object using the parameters given in the
        // homework instructions, and assign object reference to person2
        Person person2 = new Person( "Barry Obama", 987654321, false, -123.45);
        
        // Print a blank line
        System.out.println();
        
        // Display person1's information
        Person.displayInfo(person1);
        
        // Print a blank 
        System.out.println();
        
        // Display person2's information
        person2.displayInfo();
        
        // update person2's information by calling each of the
        // four instance 
        
        person2.setName("barack hussein obama");
        person2.setSocialSecurityNumber( 123456789 );
        person2.setVoterRegistrationStatus( true );
        person2.setAnnualSalary(400000.00);
        
        // Print a blank line
        System.out.println();
        
        // Print a message indicating that person2's info has been changed
        System.out.println("person 2's info has been changed to: ");
        // Print a blank line
        System.out.println();
        // Display person2's updated information 
        Person.displayInfo( person2 );
    } // end method main

} // end class PersonTest

